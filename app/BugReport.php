<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BugReport extends Model
{
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['name', 'summary', 'impact', 'details', 'os', 'browser', 'attachment', 'status_id'];

    /**
     * Project has one status
     *
     * @return Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }
}
