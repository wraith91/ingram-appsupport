<?php

namespace App;

use App\Owner;
use Illuminate\Database\Eloquent\Model;

class ProjectRequest extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'project_requests';

	/**
	 * Fillable fields for the model
	 *
	 * @var array
	 */
	protected $fillable = ['owner_id', 'status_id', 'name', 'requestor', 'problem', 'benefits', 'fs_planned', 'fs_actual', 'ce_planned', 'ce_actual', 'br_planned', 'br_actual', 'eb_usd', 'ab_usd'];

    /**
     * Project has one owner
     *
     * @return Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function owner()
    {
        return $this->hasOne(Owner::class, 'id', 'owner_id');
    }

    /**
     * Project has one status
     *
     * @return Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }
}
