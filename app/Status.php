<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
	/**
	 * Fillable fields for the model
	 *
	 * @var array
	 */
    protected $fillable = ['name'];
}
