<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => 'required',
            'summary'       => 'required',
            'problem'       => 'required|min:10',
            'opportunity'   => 'required|min:20',
            'benefits'      => 'required|min:20',
            // 'fs_planned' => 'required',
            // 'fs_actual'  => 'required',
            // 'eb_usd'     => 'required',
            // 'ce_planned' => 'required',
            // 'ce_actual'  => 'required',
            // 'br_planned' => 'required',
            // 'br_actual'  => 'required',
            // 'ab_usd'     => 'required',
            // 'attachment' => 'required|mimes:pdf,docx,doc,vsd|size:5000',
        ];



        // $attachment = count($this->input('attachment'));
        // foreach(range(0, $attachment) as $index) {
        //     $rules['attachment.' . $index] = 'mimes:pdf,docx,doc,vsd|size:5000'
        // }

        return $rules;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'fs_planned.required' => ' is required',
        ];
    }
}
