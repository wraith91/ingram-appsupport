<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Owner;
use App\ProjectRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$project_owner = Owner::pluck('name', 'id');
	    return view('app.index', compact('project_owner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $inputs = $request->except('_token', 'owner', 'attachment') + ['owner_id' => $request->get('owner')];

        $request = ProjectRequest::create($inputs);

        if ($request)
        {
            session()->flash('flash_message', 'It was successful');
        }

        return redirect()->back();
    }
}
