<?php

namespace App\Http\Controllers;

use App\Mail\SurveyPostMail;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SurveyController extends Controller
{
    private function getNTID()
    {
        if (env('USER') != 'AUTH_USER')
        {
            return 'usyerj00';
        } else {
            return substr(env('AUTH_USER'), 10);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Survey::where('ntid', $this->getNTID())->first();

        if (! isset($user)) {
            $name = 'John Doe';
            $disabled = 'disabled';
        } else {
            $name = $user->first_name;
            $disabled = $user->is_finished == 1 ? 'disabled' : '';
        }

        return view('app.survey.index', compact('name', 'disabled'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Survey::where('ntid', $this->getNTID())->first();

        if (isset($user)) {
            if ($user->is_finished == 1)
            {
                session()->flash('flash_message', "You're already done with the survey.");
                return redirect('survey');
            }
        } else {
            session()->flash('flash_message_alert', "You're not allowed to take the survey");
            return redirect('survey');
        }

        return view('app.survey.create', compact('user'));        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'q1' => 'required',
            'q2' => 'required',
            'q3' => 'required',
            'q4' => 'required',
            'q5' => 'required',
            'q6' => 'required',
            'q7' => 'required',
            'q8' => 'required',
            'q9' => 'required',
            'q10' => 'required',
            'q11' => 'required',
            'q12' => 'required',
            'q13' => 'required',
            'qc1' => 'required',
            'qc2' => 'required',
            'qc3' => 'required',
            'qc4' => 'required',
            'qc5' => 'required',
            'qc6' => 'required',
            'qc7' => 'required',
            'qc8' => 'required',
            'qc9' => 'required',
            'qc10' => 'required',
            'qc11' => 'required',
        ]);
        
        $survey = Survey::where('ntid', $request->ntid)->first();

        if ($survey->fill($request->except('_token', 'ntid') + ['is_finished' => 1])->save())
        {
            session()->flash('flash_message', 'Thank you for your time, your data has been successfully posted!');

            Mail::to($survey)->send(new SurveyPostMail($survey));
        }
        
        return redirect('survey');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        //
    }
}
