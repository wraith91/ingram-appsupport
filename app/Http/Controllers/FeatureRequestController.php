<?php

namespace App\Http\Controllers;

use App\FeatureRequest;
use Illuminate\Http\Request;

class FeatureRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = FeatureRequest::create($request->except('_token'));

        if ($request)
        {
            session()->flash('flash_message', 'It was successful');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeatureRequest  $featureRequest
     * @return \Illuminate\Http\Response
     */
    public function show(FeatureRequest $featureRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeatureRequest  $featureRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(FeatureRequest $featureRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeatureRequest  $featureRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeatureRequest $featureRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeatureRequest  $featureRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeatureRequest $featureRequest)
    {
        //
    }
}
