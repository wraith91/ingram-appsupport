<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['name'];
}
