<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
	/**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $guarded = [''];
}
