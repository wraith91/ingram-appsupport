<?php

use App\Acme\Survey;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SurveyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surveys')->truncate();

        factory(App\Survey::class)->create(['ntid' => 'ussapj01', 'first_name' => 'Eric', 'email' => 'eric.sapungan@ingrammicro.com', 'process' => 'Programmer']);
        factory(App\Survey::class)->create(['ntid' => 'usyerj00', 'first_name' => 'Julius', 'email' => 'julius.yerro@ingrammicro.com', 'process' => 'Programmer']);
        factory(App\Survey::class)->create(['ntid' => 'usdecm02', 'first_name' => 'Mark', 'email' => 'mark.decano@ingrammicro.com',  'process' => 'Director']);
        factory(App\Survey::class)->create(['ntid' => 'uspagf00', 'first_name' => 'Francis', 'process' => 'Programmer']);
        factory(App\Survey::class)->create(['ntid' => 'usrabj00', 'first_name' => 'Jeanette', 'process' => 'UX Engineer']);
        factory(App\Survey::class)->create(['ntid' => 'ushuam00', 'first_name' => 'Mike', 'email' => 'michael.huang@ingrammicro.com',  'process' => 'Team Lead']);
        factory(App\Survey::class)->create(['ntid' => 'uslarw00', 'first_name' => 'William', 'process' => 'Manager']);
        factory(App\Survey::class)->create(['ntid' => 'usamav00', 'first_name' => 'Junar', 'process' => 'GM']);
    }
}

