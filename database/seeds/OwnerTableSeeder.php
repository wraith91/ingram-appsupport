<?php

use App\Owner;
use Illuminate\Database\Seeder;

class OwnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('owners')->truncate();

        Owner::insert([
        	['name' => 'PSS'],
        	['name' => 'Sales'],
        	['name' => 'Global'],
        	['name' => 'Finance'],
        	['name' => 'Purchasing'],
        	['name' => 'Go to Market'],
        	['name' => 'Support - BPI'],
        	['name' => 'Support - PST'],
        	['name' => 'Support - TDD'],
        	['name' => 'Support - Admin'],
        	['name' => 'Support - Quality'],
        	['name' => 'Support - Services'],
        	['name' => 'Support - Facilities'],
        	['name' => 'Support - Purchasing'],
        	['name' => 'Support - Recruitment'],
        	['name' => 'Support - Transitions'],
        	['name' => 'Strategic Operations'],
        	['name' => 'Share Services - Sofia'],
        	['name' => 'Share Services - Costa Rica'],
        ]);
    }
}
