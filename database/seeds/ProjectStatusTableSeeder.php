<?php

use App\Status;
use Illuminate\Database\Seeder;

class ProjectStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->truncate();

        Status::insert([
        	['name' => 'Pending'],
        	['name' => 'Approved'],
            ['name' => 'Rejected'],
            ['name' => 'Complete'],
        	['name' => 'In Progress'],
        ]);
    }
}
