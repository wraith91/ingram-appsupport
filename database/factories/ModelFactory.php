<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Survey::class, function (Faker\Generator $faker) {

    return [
        'ntid'       => 'us' . strtolower(str_random(3)) . '00',
        'first_name' => $faker->firstName,
        'email'      => $faker->unique()->safeEmail,
        'process'    => $faker->word,
        'country'    => $faker->countryCode,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\ProjectRequest::class, function (Faker\Generator $faker) {

    return [
        'owner_id'   => 1,
        'name'       => $faker->company,
        'requestor'  => 'us' . strtolower(str_random(3)) . '00',
        'problem'    => $faker->sentence,
        'benefits'   => $faker->catchPhrase,
        // 'attachment' => $faker->name . '.vsd',
    ];

});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\BugReport::class, function (Faker\Generator $faker) {

    return [
        'name'       => $faker->company,
        'summary'    => $faker->catchPhrase,
        'impact'     => 'LOW',
        'details'    => $faker->sentence,
        'os'         => 'Windows',
        'browser'    => 'CHROME',
        'attachment' => $faker->name . $faker->fileExtension,
    ];

});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\FeatureRequest::class, function (Faker\Generator $faker) {

    return [
        'name'    => $faker->company,
        'summary' => $faker->catchPhrase,
        'problem' => $faker->sentence
    ];

});