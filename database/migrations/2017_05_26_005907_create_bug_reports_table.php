<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBugReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bug_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('summary');
            $table->string('impact', 4);
            $table->text('details');
            $table->string('os', 15);
            $table->string('browser', 20);
            $table->string('attachment')->nullable();
            $table->integer('status_id')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('status_id')
                  ->references('id')
                  ->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bug_reports');
    }
}
