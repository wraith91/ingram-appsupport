<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->integer('status_id')->unsigned()->default(1);
            $table->string('name', 50);
            $table->string('requestor', 11);
            $table->text('problem');

            // Financial Savings
            $table->string('fs_planned', 10)->nullable();
            $table->string('fs_actual', 10)->nullable();

            // Compliance Efficiencies
            $table->string('ce_planned', 10)->nullable();
            $table->string('ce_actual', 10)->nullable();

            // Budget Reduction
            $table->string('br_planned', 10)->nullable();
            $table->string('br_actual', 10)->nullable();

            // Expected Benefits USD
            $table->string('eb_usd', 10)->nullable();

            // Approved Budget USD
            $table->string('ab_usd', 10)->nullable();

            $table->text('benefits');
            // $table->string('attachment');
            $table->timestamps();

            $table->foreign('owner_id')
                  ->references('id')
                  ->on('owners');

            $table->foreign('status_id')
                  ->references('id')
                  ->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_requests');
    }
}
