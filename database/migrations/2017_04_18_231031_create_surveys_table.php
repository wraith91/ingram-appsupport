<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ntid', 10);
            $table->string('first_name', 20);
            $table->string('country')->nullable();
            $table->string('process')->nullable();
            $table->string('email');
            $table->string('q1')->nullable();
            $table->string('q2')->nullable();
            $table->string('q3')->nullable();
            $table->string('q4')->nullable();
            $table->string('q5')->nullable();
            $table->string('q6')->nullable();
            $table->string('q7')->nullable();
            $table->string('q8')->nullable();
            $table->string('q9')->nullable();
            $table->string('q10')->nullable();
            $table->string('q11')->nullable();
            $table->string('q12')->nullable();
            $table->string('q13')->nullable();
            $table->text('qc1')->nullable();
            $table->text('qc2')->nullable();
            $table->text('qc3')->nullable();
            $table->text('qc4')->nullable();
            $table->text('qc5')->nullable();
            $table->text('qc6')->nullable();
            $table->text('qc7')->nullable();
            $table->text('qc8')->nullable();
            $table->text('qc9')->nullable();
            $table->text('qc10')->nullable();
            $table->text('qc11')->nullable();
            $table->boolean('is_finished')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
