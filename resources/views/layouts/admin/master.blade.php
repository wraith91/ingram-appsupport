<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Relic</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" id="bulma" href="{{ asset('main/css/vendor/bulma.css') }}" />
    <link rel="stylesheet" href="{{ asset('main/css/admin.css') }}" />
  </head>
  <body>
    <nav class="nav has-shadow" id="top">
    <div class="container">
      <div class="nav-left">
        <a class="nav-item" href="../index.html">
          Relic
        </a>
      </div>
      <span class="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
      </span>
      <div class="nav-right nav-menu">
        <a href="/admin/dashboard" class="nav-item is-tab is-{{ set_active(['admin/dashboard']) }}">
          Dashboard
        </a>
        <a href="/admin/projects" class="nav-item is-tab is-{{ set_active(['admin/projects/*']) }}">
          Project Requests
        </a>
        <a href="/admin/bugs" class="nav-item is-tab is-{{ set_active(['admin/bugs']) }}">
          Bug Reports
        </a>
        <a href="/admin/features" class="nav-item is-tab is-{{ set_active(['admin/features']) }}">
          Feature Requests
        </a>
        <!-- <span class="nav-item">
          <a class="button">
            Log in
          </a>
          <a class="button is-info">
            Sign up
          </a>
        </span> -->
      </div>
    </div>
    </nav>
    <div class="section">
      @yield('content')
    </div>

  <!-- Start Demo Javascript -->
  <script src="{{ asset('main/js/jquery.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js"></script>
  <script src="{{ asset('main/js/vendor/bulma.js') }}"></script>
  <script src="{{ asset('main/js/vendor/board.js') }}"></script>
  @yield('footer')
  <!-- End Demo Javascript -->
  </body>
</html>