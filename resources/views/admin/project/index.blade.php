@extends('layouts.admin.master')

@section('content')
  <div id="dashboard">
    <div class="boards">
      <div class="columns">
		<div class="column is-8 is-offset-2 board" id="b1">
			<div class="box is-gray">
				<div class="head">
				  <div class="name">Project Requests</div>
				  <div class="count">@{{lists.count}}</div>
				  <div class="options" style="padding-top: 0;">
				  	<div class="field is-horizontal">
					  <div class="field-label is-normal">
					    <label class="label" style="width: 130px;">Sort by Status:</label>
					  </div>
					  <div class="field-body">
					    <div class="field is-narrow">
					      <div class="control">
					        <div class="select is-fullwidth">
					          <select v-on:change="filter">
					            <option v-for="status in object" v-bind:value="status.id" >
									@{{ status.name }}
								</option>
					          </select>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
    			</div>
				</div>
				<div class="items">
					{{-- @foreach($projects as $key => $project) --}}
				  		<div class="box" v-for="list in lists.data" v-bind:id="'b1c' + list.id">
						    <p class="title">@{{list.name}} <span class="unstyled">owned by @{{list.owner}}</span></p>
						    <p class="body">@{{list.problem}}</p>
						    <p class="title"></p>
						    <div class="meta">
						      {{-- <img src="https://placehold.it/28x28"> --}}
						      <div>
						      	<a v-bind:href="'http://appsupport.dev/admin/projects/' + list.id" title="View Project"><i class="fa fa-eye"></i></a>
						      </div>
						      <div><span class="created">Posted @{{list.created_at}}</span></div>
						    </div>
					  	</div>
					{{-- @endforeach --}}
{{-- 				<div class="add-card">
					<div onclick="addCard(this)">
						<i class="fa fa-plus-circle"></i>
						Add new card
					</div>
				</div> --}}
				</div>
			</div>
		</div>
{{-- 		<div class="column is-4 board" id="b1">
			<div class="box is-gray">
				<div class="head">
				  <div class="name">Feature Requests</div>
				  <div class="count">{{$features->count()}}</div>
				  <div class="options"><i class="fa fa-ellipsis-h"></i></div>
				</div>
				<div class="items">
					@foreach($features as $key => $feature)
				  		<div class="box" id="b1c{{$key+1}}">
						    <p class="title">{{$feature->name}}</p>
						    <p class="meta">
						      <img src="https://placehold.it/28x28">
						      <a href="#"><i class="fa fa-bars"></i></a>
						    </p>
					  	</div>
					@endforeach
				<div class="add-card">
					<div onclick="addCard(this)">
						<i class="fa fa-plus-circle"></i>
						Add new card
					</div>
				</div>
				</div>
			</div>
		</div> --}}
{{-- 		<div class="column is-4 board" id="b1">
			<div class="box is-gray">
				<div class="head">
				  <div class="name">Bug Reports</div>
				  <div class="count">{{$bugs->count()}}</div>
				  <div class="options"><i class="fa fa-ellipsis-h"></i></div>
				</div>
				<div class="items">
					@foreach($bugs as $key => $bug)
				  		<div class="box" id="b1c{{$key+1}}">
						    <p class="title">{{$bug->name}}</p>
						    <p class="meta">
						      <img src="https://placehold.it/28x28">
						      <a href="#"><i class="fa fa-bars"></i></a>
						    </p>
					  	</div>
					@endforeach
				<div class="add-card">
					<div onclick="addCard(this)">
						<i class="fa fa-plus-circle"></i>
						Add new card
					</div>
				</div>
				</div>
			</div>
		</div> --}}
    </div>
  </div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.3/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script>
		var app = new Vue({
			el: '#dashboard',

			data: {
           		selected: 1,
				query: '',
				object: {},
				lists: {},
			},

			created() {
				var url = 'http://appsupport.dev/api/status';

	        	axios.get(url).then((response) => {
				  this.object = response.data;
				  console.log(this.object);
				});

				var list = 'http://appsupport.dev/api/project/request?status=1';

	        	axios.get(list).then((response) => {
				  this.lists = response.data;
				  console.log(this.lists);
				});
			},

			methods: {
				// Search Associate for autocomplete
				filter: function (event) {
					var url = 'http://appsupport.dev/api/project/request?status=' + event.target.value;
		        	axios.get(url).then((response) => {
					  this.lists = response.data;
					});
				}
			}
		});
	</script>
</script>
@endsection