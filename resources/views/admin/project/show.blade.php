@extends('layouts.admin.master')

@section('content')
  <div id="dashboard">
    <div class="boards">
      <div class="columns">
		<div class="column is-8 is-offset-2 board" id="b1">
			<div class="box is-gray">
				<div class="head">
				  <div class="name">Project Requests</div>
				  <div class="count">@{{lists.count}}</div>
				  <div class="options" style="padding-top: 0;">
				  	<div class="field is-horizontal">
					  <div class="field-label is-normal">
					    <label class="label" style="width: 130px;">Sort by Status:</label>
					  </div>
					  <div class="field-body">
					    <div class="field is-narrow">
					      <div class="control">
					        <div class="select is-fullwidth">
					          <select v-on:change="filter">
					            <option v-for="status in object" v-bind:value="status.id" >
									@{{ status.name }}
								</option>
					          </select>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
    			</div>
				</div>
				<div class="items">
			  		<div class="box" v-for="list in lists.data" v-bind:id="'b1c' + list.id">
					    <p class="title">@{{list.name}} <span class="unstyled">owned by @{{list.owner}}</span></p>
					    <p class="body">@{{list.problem}}</p>
					    <p class="title"></p>
					    <div class="meta">
					      {{-- <img src="https://placehold.it/28x28"> --}}
					      <div>
					      	<a v-bind:href="'http://appsupport.dev/admin/projects/' + list.id" title="View Project"><i class="fa fa-eye"></i></a>
					      </div>
					      <div><span class="created">Posted @{{list.created_at}}</span></div>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
    </div>
  </div>
@endsection