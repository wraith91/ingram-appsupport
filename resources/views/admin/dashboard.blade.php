@extends('layouts.admin.master')

@section('content')
  <div id="bug">
    <div class="boards">
      <div class="columns">
		<div class="column is-4 board" id="b1">
			<div class="box is-gray">
				<div class="dash-content">
					<div class="card-title">
						Project Request
					</div>
					<h2>@{{lists.project}}</h2>
					<h6>Total Pending</h6>
				</div>
			</div>
		</div>
		<div class="column is-4 board" id="b1">
			<div class="box is-gray">
				<div class="dash-content">
					<div class="card-title">
						Feature Request
					</div>
					<h2>@{{lists.feature}}</h2>
					<h6>Total Pending</h6>
				</div>
			</div>
		</div>
		<div class="column is-4 board" id="b1">
			<div class="box is-gray">
				<div class="dash-content">
					<div class="card-title">
						Bug Report
					</div>
					<h2>@{{lists.bug}}</h2>
					<h6>Total Pending</h6>
				</div>
			</div>
		</div>
    </div>
  </div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.3.3/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script>
		var app = new Vue({
			el: '#bug',

			data: {
				lists: {},
			},

			created() {
				var url = 'http://appsupport.dev/api/status';

	        	axios.get(url).then((response) => {
				  this.object = response.data;
				  console.log(this.object);
				});

				var list = 'http://appsupport.dev/api/dashboard/reports';

	        	axios.get(list).then((response) => {
				  this.lists = response.data;
				  console.log(this.lists);
				});
			},
		});
	</script>
</script>
@endsection