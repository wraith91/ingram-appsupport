<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Portfolio</h2>
            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Sphere</h4>
                <p class="text-muted">WFM</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>HR Portal</h4>
                <p class="text-muted">Human Resource</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Finance Portal v1 and v2</h4>
                <p class="text-muted">Finance</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Fully Burdened Cost</h4>
                <p class="text-muted">Finance</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>QUBE</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Talent League</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Budget Portal</h4>
                <p class="text-muted">Finance</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Exit Module</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Scorecard Module</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Travel Portal</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                <div class="portfolio-hover">
                    <div class="portfolio-hover-content">
                        <i class="fa fa-plus fa-3x"></i>
                    </div>
                </div>
                <img src="{{ asset('main/images/portfolio/roundicons.png') }}" class="img-responsive" alt="">
            </a>
            <div class="portfolio-caption">
                <h4>Mobile App</h4>
                <p class="text-muted">Ingram Philippines</p>
            </div>
        </div>
    </div>
</div>