 <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">"Here are the highlights within the team"</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="{{ asset('main/images/about/1.jpg') }}" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Jan-2016</h4>
                                    <h4 class="subheading">Our Humble Beginnings</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Business Solutions Team was created under Mark Decano with Mike Huang as the lone wolf.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="{{ asset('main/images/about/2.jpg')}}" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>July 2016</h4>
                                    <h4 class="subheading">Establishment</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Mike was promoted as Lead and the idea of hiring more was discussed during the Business
                                    Operations Team Outing</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="{{ asset('main/images/about/3.jpg')}}" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>August 2016</h4>
                                    <h4 class="subheading">Hiring Process</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">The process of hiring new heads started and the first one is Charmaine who is tasked
                                    to create the Android Mobile development. Francis onboarded as an OJT around that time.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="{{ asset('main/images/about/4.jpg')}}" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>Dec 2016</h4>
                                    <h4 class="subheading">Rebrand</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">With the upcoming new hires the Apps Support Team was created to have a clear understanting
                                    what the team will do moving forward.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="{{ asset('main/images/about/4.jpg')}}" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>March 2017</h4>
                                    <h4 class="subheading">Equiped for Battle</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">The team roster has been completed, with the hiring of Francis (from OJT), Eric and Julis
all under Back End Development and Jeanette who is tasked to work on Front End Design.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be Part
                                    <br>Of Our
                                    <br>Story!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>