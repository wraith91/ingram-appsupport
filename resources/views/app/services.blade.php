<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Services</h2>
            <h3 class="section-subheading text-muted">"Here are the high-level skills of the team"</h3>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-md-3">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-desktop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Web Development</h4>
            <p class="text-muted">The team can help you create sites ranging from simple static page to complex sites such as e-commerce, ticketing system and dashboards.
Easy to deploy, just hit refresh and you have the updated version of the site.</p>
        </div>
        <div class="col-md-3">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Mobile Development</h4>
            <p class="text-muted">The new kid from the block. Mobile development is new to the center and has the potential to impact everyone in the center.
Since most associate has a smartphone these days.</p>
        </div>
        <div class="col-md-3">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-connectdevelop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Sharepoint Design</h4>
            <p class="text-muted">SharePoint is good in quick projects deployment,
with built-in Workflow and Email Notification.</p>
        </div>
        <div class="col-md-3">
            <span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                <i class="fa fa-cubes fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">VBA Macro</h4>
            <p class="text-muted">Only one from the team can do this but, why not. This skill is limited to VBA Excel.</p>
        </div>
    </div>
</div>