<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Project Requisition</h2>
            <h3 class="section-subheading text-muted">How can we help you? Got some ideas in mind? Shoot us your ideas and we'll make it happen!</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a data-toggle="tab" href="#tab1"><h5>Post a Project</h5></a></li>
                <li><a data-toggle="tab" href="#tab2"><h5>Bug Report</h5></a></li>
                <li><a data-toggle="tab" href="#tab3"><h5>Feature Request</h5></a></li>
            </ul>

            <div class="tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    {!! Form::open(['route' => 'project', 'novalidate']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- pr_name Field -->
                                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Project Name *', 'data-validation-required-message' => 'What is the project name?', 'required']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <!-- Requestor Field -->
                                    {!! Form::text('requestor', 'usyerj00', ['id' => 'requestor', 'class' => 'form-control', 'readonly' => 'readonly']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- Owner Field -->
                                    {{ Form::select('owner', $project_owner, null, ['placeholder' => 'Select Owner *', 'class' => 'form-control', 'id' => 'name']) }}
                                </div>
                                <div class="form-group">
                                    <!-- Summary Field -->
                                    {!! Form::textarea('summary', null, ['id' => 'summary', 'class' => 'form-control', 'placeholder' => 'Project Summary *', 'data-validation-required-message' => 'What is the summary of this project?', 'required']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('summary') }}</p>
                                </div>
                                <div class="form-group">
                                    <!-- Problem Field -->
                                    {!! Form::textarea('problem', null, ['id' => 'problem', 'class' => 'form-control', 'placeholder' => 'Project Problem / Opportunity *', 'data-validation-required-message' => 'What is the main problem of this project?', 'required']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('problem') }}</p>
                                </div>
                                <div class="form-group">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td class="thead">Planned</td>
                                                <td class="thead">Actual</td>
                                                <td class="no-border-top"></td>
                                                <td class="thead">Expected Benefits *</td>
                                            </tr>
                                            </tr>
                                            <tr>
                                                <td class="thead">Financial Savings *</td>
                                                <td class="c-body">
                                                    {!! Form::number('fs_planned', null, ['id' => 'requestor', 'class' => 'form-control']) !!}
                                                    <p class="help-block text-danger">{{ $errors->first('fs_planned') }}</p>
                                                </td>
                                                <td class="c-body">{!! Form::number('fs_actual', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                                <td class="no-border-top"></td>
                                                <td>{!! Form::number('eb_usd', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td class="thead">Business Efficiency</td>
                                                <td>{!! Form::number('ce_planned', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                                <td>{!! Form::number('ce_actual', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                                <td class="no-border-top"></td>
                                                <td class="thead">Approved Budget USD *</td>
                                            </tr>
                                            <tr>
                                                <td class="thead">Customer Satisfaction</td>
                                                <td>{!! Form::number('br_planned', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                                <td>{!! Form::number('br_actual', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                                <td class="no-border-top"></td>
                                                <td>{!! Form::number('ab_usd', null, ['id' => 'requestor', 'class' => 'form-control']) !!}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group">
                                    <!-- Benefits Field -->
                                    {!! Form::textarea('benefits', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Project Benefits *', 'data-validation-required-message' => 'To whom it would be beneficial and what are the benefits of this project?', 'required']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('benefits') }}</p>
                                </div>
                                <div class="form-group">
                                    <!-- Attachments Field -->
                                    {!! Form::file('attachment[]',['class' => 'form-control', 'multiple']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('attachment') }}</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Post Project</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div id="tab2" class="tab-pane fade">
                    {!! Form::open(['route' => 'bug', 'enctype' => 'multipart/form-data', 'novalidate']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- pr_name Field -->
                                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Project Name *', 'data-validation-required-message' => 'What is the project name?', 'required']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- Summary Field -->
                                    {!! Form::textarea('summary', null, ['id' => 'summary', 'class' => 'form-control', 'placeholder' => 'Bug Summary *', 'data-validation-required-message' => 'What is the summary of this project?', 'required']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- Impact Field -->
                                    {{ Form::select('impact', ['Low' => 'Low', 'Mid' => 'Mid', 'High' => 'High'], null, ['placeholder' => 'What is the impact *', 'class' => 'form-control', 'id' => 'name']) }}
                                </div>
                                <div class="form-group">
                                    <!-- Details Field -->
                                    {!! Form::textarea('details', null, ['id' => 'problem', 'class' => 'form-control', 'placeholder' => 'Bug Details *', 'data-validation-required-message' => 'What are the details of the bug', 'required']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- OS Field -->
                                    {{ Form::select('os', ['Windows' => 'Windows', 'Linux' => 'Linux', 'Apple' => 'Apple', 'IOS' => 'IOS', 'Android' => 'Android'], null, ['placeholder' => 'What os used? *', 'class' => 'form-control', 'id' => 'name']) }}
                                </div>
                                <div class="form-group">
                                    <!-- Browser Field -->
                                    {{ Form::select('browser', ['Chrome' => 'Chrome', 'Firefox' => 'Firefox', 'Edge' => 'Edge', 'Internet Explorer' => 'Internet Explorer'], null, ['placeholder' => 'What browser used? *', 'class' => 'form-control', 'id' => 'name']) }}
                                </div>
                                <div class="form-group">
                                    <!-- Attachments Field -->
                                    {!! Form::file('attachment',['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Send Bug Issue</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div id="tab3" class="tab-pane fade">
                    {!! Form::open(['route' => 'feature']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- pr_name Field -->
                                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Project Name *', 'data-validation-required-message' => 'What is the project name?', 'required']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- Summary Field -->
                                    {!! Form::textarea('summary', null, ['id' => 'summary', 'class' => 'form-control', 'placeholder' => 'Feature Summary *', 'data-validation-required-message' => 'What is the summary of this feature?', 'required']) !!}
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- Problem Field -->
                                    {!! Form::textarea('problem', null, ['id' => 'problem', 'class' => 'form-control', 'placeholder' => 'Feature Problem *', 'data-validation-required-message' => 'What is the main problem to solve by this feature?', 'required']) !!}
                                    <p class="help-block text-danger">{{ $errors->first('problem') }}</p>
                                </div>
                                <div class="form-group">
                                    <!-- Attachments Field -->
                                    {!! Form::file('attachment[]',['class' => 'form-control', 'multiple']) !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Submit Request </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer')

    <!-- CKEditor JavaScript -->
    <script src="{{ asset('main/js/vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'benefits' );
    </script>

    <link href="http://ned.im/noty/vendor/noty-3/noty.css" rel="stylesheet">
    <script src="{{ asset('main/js/vendor/noty.js') }}"></script>

    @if(Session::has('flash_message'))
        <script>
            new Noty({
                  type: 'success',
                  layout: 'bottomRight',
                  theme: 'mint',
                  text: 'Your request has been submitted!',
                  timeout: 5000,
                  progressBar: true,
                  // closeWith: ['click', 'button'],
                  // animation: {
                  //   open: 'noty_effects_open',
                  //   close: 'noty_effects_close'
                  // },
                  // id: false,
                  // force: false,
                  // killer: false,
                  // queue: 'global',
                  // container: false,
                  // buttons: [],
                  // sounds: {
                  //   sources: [],
                  //   volume: 1,
                  //   conditions: []
                  // },
                  // titleCount: {
                  //   conditions: []
                  // },
                  // modal: false
            }).show();
        </script>
    @endif


@endsection