<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MSSC Customer Survey</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/prettify.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.1.0/css/bulma.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/statuspage.css') }}">
  <style>
  	.error {
  		color: red;
	}
  </style>
</head>
	<body>
	<div class="container">
	    <div class="logo">
	      <a href="/">
	        <img src="{{ asset('main/image/logos/ingram_logo.png') }}">
	      </a>
	    </div>
	    <div class="section">
			<div class="logo" style="padding: 20px;">
				<img src="{{ asset('main/image/logos/survey_logo.png') }}" style="height: 180px; width: 250px; margin: 0 auto; display: block;">
			</div>
			<hr>
			
			<div class="status-list">
				{!! Form::open(['id' => 'surveyForm', 'url' => 'survey']) !!}
				<input type="hidden" name="ntid" value="{{$user->ntid}}">
				<div id="rootwizard">

					@if ($errors->any())
						<div id="notification" class="notification is-warning">
							Please complete answering all the questions.
						</div>
					@endif

					<div class="tabs is-centered is-small">
						<ul style="border-bottom: 0;">
							<li><a href="#tab1" data-toggle="tab" style="border-bottom: 0; cursor: default;">PEOPLE AND PERFORMANCE</a></li>
							<li><a href="#tab2" data-toggle="tab" style="border-bottom: 0; cursor: default;">REPORTS &amp ANALYTICS</a></li>
							<li><a href="#tab3" data-toggle="tab" style="border-bottom: 0; cursor: default;">SUPPORT TEAMS</a></li>
							<li><a href="#tab4" data-toggle="tab" style="border-bottom: 0; cursor: default;">CUSTOMER EXPERIENCE</a></li>
							<li><a href="#tab5" data-toggle="tab" style="border-bottom: 0; cursor: default;">SURVEY</a></li>
						</ul>
					</div>


					<div class="tab-content">
						
					    <div class="tab-pane" id="tab1">
							<div class="survey-item">
								<h4 class="survey-label">1.) Associates from Manila Shared Services Center thoroughly know and understand the process.</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q1">
									<input type="radio" name="q1" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q1" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q1" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q1" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q1" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q1" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q1" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q1" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q1" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q1" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q1" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc1" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">2.) Associates from Manila Shared Services Center thoroughly know and understand the process.</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q2">
									<input type="radio" name="q2" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q2" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q2" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q2" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q2" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q2" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q2" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q2" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q2" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q2" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q2" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc2" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">3.) Associates from Manila Shared Services Center understand my business needs.</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q3">
									<input type="radio" name="q3" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q3" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q3" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q3" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q3" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q3" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q3" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q3" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q3" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q3" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q3" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc3" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">4.) How satisfied are you with our Team Leaders’ overall level of effectiveness?</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q4">
									<input type="radio" name="q4" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q4" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q4" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q4" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q4" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q4" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q4" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q4" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q4" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q4" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q4" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc4" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">5.) How satisfied are you with our Operations Manager's overall level of effectiveness?</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q5">
									<input type="radio" name="q5" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q5" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q5" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q5" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q5" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q5" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q5" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q5" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q5" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q5" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q5" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc5" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">6.) How satisfied are you with our Executive Team's overall level of effectiveness?</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q6">
									<input type="radio" name="q6" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q6" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q6" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q6" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q6" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q6" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q6" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q6" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q6" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q6" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q6" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc6" required></textarea>
									</p>
								</div>	
							</div>
					    </div>

					    <div class="tab-pane" id="tab2">	
							<div class="survey-item">
								<h4 class="survey-label">7.) The quality of reports that you use in Ingram Micro Manila Shared Services is sufficient for our requirements.</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q7">
									<input type="radio" name="q7" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q7" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q7" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q7" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q7" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q7" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q7" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q7" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q7" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q7" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q7" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc7" required></textarea>
									</p>
								</div>	
							</div>
					    </div>

					    <div class="tab-pane" id="tab3">
							<div class="survey-item">
								<h4 class="survey-label">8.) Does the Service Quality team effective in ensuring quality of work in the Manila Shared Services Center?</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q8">
									<input type="radio" name="q8" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q8" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q8" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q8" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q8" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q8" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q8" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q8" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q8" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q8" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q8" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc8" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">9.) Manila has an effective leadership program for new leaders</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q9">
									<input type="radio" name="q9" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q9" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q9" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q9" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q9" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q9" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q9" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q9" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q9" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q9" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q9" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc9" required></textarea>
									</p>
								</div>	
							</div>
					    </div>

					    <div class="tab-pane" id="tab4">
							<div class="survey-item">
								<h4 class="survey-label">10.) On an overall basis, how satisfied are you with Ingram Micro Manila Shared Services?</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q10">
									<input type="radio" name="q10" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q10" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q10" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q10" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q10" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q10" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q10" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q10" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q10" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q10" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q10" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc10" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">11.) I would recommend Ingram Micro Manila Shared Services to my peers and business partners.</h4>
								<div class="survey-rating">
									<div class="survey-rating-title">Not at all likely</div>
									<div class="survey-rating-title">Extremely likely</div>
								</div>
								<label for="q11">
									<input type="radio" name="q11" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="1" name="q11" required>1
									</label>
									<label class="radio">
										<input type="radio"  value="2" name="q11" required >2
									</label>
									<label class="radio">
										<input type="radio"  value="3" name="q11" required >3
									</label>
									<label class="radio">
										<input type="radio"  value="4" name="q11" required>4
									</label>
									<label class="radio">
										<input type="radio"  value="5" name="q11" required>5
									</label>
									<label class="radio">
										<input type="radio"  value="6" name="q11" required>6
									</label>
									<label class="radio">
										<input type="radio"  value="7" name="q11" required>7
									</label>
									<label class="radio">
										<input type="radio"  value="8" name="q11" required>8
									</label>
									<label class="radio">
										<input type="radio"  value="9" name="q11" required>9
									</label>
									<label class="radio">
										<input type="radio"  value="10" name="q11" required>10
									</label> 	
								</div>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="qc11" required></textarea>
									</p>
								</div>	
							</div>
					    </div>

					    <div class="tab-pane" id="tab5">
							<div class="survey-item">
								<h4 class="survey-label">12.) Please provide other comments that you may think will be helpful to improve Manila Shared Services Center.</h4>
								<div class="field">
									<p class="control">
										<textarea class="textarea" placeholder="Comment" name="q12" required></textarea>
									</p>
								</div>	
							</div>

							<hr>
							
							<div class="survey-item">
								<h4 class="survey-label">13.) Can we contact you on the phone regarding your reply?</h4>
								<label for="q13">
									<input type="radio" name="q13" style="visibility: hidden; margin-bottom: 10px; width: 0px;" required>
								</label>
								<div class="radios">
									<label class="radio">
										<input type="radio"  value="Yes" name="q13" required>Yes
									</label>
									<label class="radio">
										<input type="radio"  value="No" name="q13" required >No
									</label>
								</div>
							</div>
					    </div>

						{{-- Pagination --}}
						<ul class="pager wizard">
							<li class="previous"><a href="javascript:;">Previous</a></li>
						  	<li class="next"><a href="javascript:;">Next</a></li>
							<li class="finish"><a href="javascript:;">Finish</a></li>
						</ul>
					</div>

				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<!-- Javascript -->

    <script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="{{ asset('main/js/bootstrap.min2.js') }}"></script>
	<script src="{{ asset('main/js/jquery.bootstrap.wizard.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="{{ asset('main/js/prettify.js') }}"></script>
	<script>
		$("#notification").show().delay(10000).fadeOut();
	</script>
	<script>
	$(document).ready(function() {
		var $validator = $("#surveyForm").validate({
		  rules: {
		    qc1: {
		      required: true,
		      minlength: 3
		    },
		    qc2: {
		      required: true,
		      minlength: 3
		    },
		    qc3: {
		      required: true,
		      minlength: 3
		    },
		    qc4: {
		      required: true,
		      minlength: 3
		    },
		    qc5: {
		      required: true,
		      minlength: 3
		    },
		    qc6: {
		      required: true,
		      minlength: 3
		    },
		    qc7: {
		      required: true,
		      minlength: 3
		    },
		    qc8: {
		      required: true,
		      minlength: 3
		    },
		    qc9: {
		      required: true,
		      minlength: 3
		    },
		    qc10: {
		      required: true,
		      minlength: 3
		    },
		    qc11: {
		      required: true,
		      minlength: 3
		    },
		    q13: {
		      required: true,
		    },
		  }
		});

	  	$('#rootwizard').bootstrapWizard({
	  		'tabClass': 'nav nav-pills',
	  		'onNext': function(tab, navigation, index) {
	  			var $valid = $("#surveyForm").valid();
	  			if(!$valid) {
	  				$validator.focusInvalid();
	  				return false;
	  			}
	  		},

	  	});
		$('#rootwizard .finish').click(function() {
			var $valid = $("#surveyForm").valid();
	  			if(!$valid) {
	  				$validator.focusInvalid();
	  				return false;
	  			}
			$('#surveyForm').submit();
		});
		window.prettyPrint && prettyPrint()
	});
	</script>
	</body>
</html>