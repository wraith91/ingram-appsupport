<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MSSC Customer Survey</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.1.0/css/bulma.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/statuspage.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/main.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/jquery.steps.css') }}">
</head>
<body>
<div class="container">
    <div class="logo">
      <a href="/ingram-appsupport/public/survey">
        <img src="{{ asset('main/image/logos/ingram_logo.png') }}">
      </a>
    </div>

    <div class="section">
		<div class="logo">
			<img src="{{ asset('main/image/logos/survey_logo.png') }}" style="height: 180px; width: 250px; margin: 0 auto; display: block;">
		</div>
		<hr>
		<div class="updates"  style="margin-bottom: 40px; padding: 40px 40px 20px 40px;">
			<div class="update">
				<h1 class="title has-text-centered">Hi, {{ $name }}</h1>
				<h3 class="introduction has-text-centered">Thank you for taking part of this survey as we hear your thoughts on how the Manila Shared Services team partners with you. This should not take longer than 5 minutes to complete but will definitely yield years in better partnerships.</h3>
				<h3 class="introduction has-text-centered"> Your responses will be taken with strict discernment. Opportunities will be addressed and wins will be celebrated.</h3>
			</div>
			@if (Session::has('flash_message'))
				<div id="notification" class="notification is-success">
					{!! Session::get('flash_message') !!}
				</div>
			@endif
      @if (Session::has('flash_message_alert'))
        <div id="notification" class="notification is-danger">
          {!! Session::get('flash_message_alert') !!}
        </div>
      @endif
			<div class="field has-text-centered" style="margin-top: 40px;">
				<a href="survey/create" class="button is-medium is-primary" {{ $disabled }}>Take the survey</a>
			</div>
		</div>
	</div>
  </div>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
  <script src="{{ asset('main/js/jquery.steps-1.1.0/jquery.steps.min.js') }}"></script>

  <script>
	$("#notification").show().delay(10000).fadeOut();
  </script>
{{--   <script src="{{ asset('main/js/jquery.min.js') }}"></script>
  <script src="{{ asset('main/js/jquery.steps.min.js') }}"></script> --}}
</body>
</html>