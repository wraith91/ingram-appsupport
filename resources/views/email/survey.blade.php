<!doctype html>
<html class="no-js" lang="">
    <head>
        <title></title>
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <strong>Hi, {{ $user->first_name }}</strong> <br><br>

		Thank you for taking time out to participate in our survey.  <br>
		We truly value the information you have provided.  <br>
		Your responses are vital in helping MSSC (Manila Shared Services Center) to provide highest standards of excellence.<br><br>

		From,<br>
		Manila Shared Services Center Customer Experience Survey
    </body>
</html>