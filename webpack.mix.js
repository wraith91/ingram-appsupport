const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/main/css')
	.sass('resources/assets/sass/admin.scss', 'public/main/css')
	.js('node_modules/jquery/dist/jquery.min.js', 'public/main/js')
	.js('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'public/main/js')
	.copy('resources/assets/js/vendor', 'public/main/js/vendor', false)
	.copy('resources/assets/image', 'public/main/images', false)

	.copy('resources/assets/components/noty/lib/noty.css', 'public/main/css/vendor', false)
	.copy('resources/assets/components/noty/lib/noty.js', 'public/main/js/vendor', false)

	.copy('resources/assets/components/bulma/css/bulma.css', 'public/main/css/vendor', false);
