<?php

use App\Owner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectController@index');
Route::post('/project/request', ['as' => 'project', 'uses' => 'ProjectController@store']);
Route::post('/feature/request', ['as' => 'feature', 'uses' => 'FeatureRequestController@store']);
Route::post('/bug/report', ['as' => 'bug', 'uses' => 'BugReportController@store']);

Route::get('/survey', ['as' => 'survey', 'uses' => 'SurveyController@index']);
Route::get('/survey/create', 'SurveyController@create');
Route::post('/survey', 'SurveyController@store');

Route::get('/test', function () {
	return env('AUTH_USER');
});

// Admin View
Route::group(['prefix' => 'admin'], function () {
	Route::get('dashboard', function () {
		return view('admin.dashboard');
	});
	Route::get('projects', function () {
		return view('admin.project.index');
	});
	Route::get('projects/{id}', function ($id) {
		$project = App\ProjectRequest::find($id);
		return view('admin.project.show', compact('project'));
	});
	Route::get('bugs', function () {
		return view('admin.bug.index');
	});
	Route::get('features', function () {
		return view('admin.feature.index');
	});
});