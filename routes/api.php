<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/status', function (Request $request) {
    $search = trim($request->get('q'));

    $results = App\Status::whereIn('id', [1,2,3])->get();
    $response = [];

    foreach ($results as $status) {
        $response[] = [
            'id' => $status->id,
            'name' => $status->name,
        ];
    }

    return response()->json($response);
});

Route::get('/project/request', function (Request $request = null) {
    $search = trim($request->get('status'));

    $results = App\ProjectRequest::with(['status', 'owner'])->where('status_id', $search);
    $items = [];

    foreach ($results->get() as $list) {
        $items[] = [
            'id' => $list->id,
            'name' => $list->name,
            'requestor' => $list->requestor,
            'owner' => $list->owner->name,
            'problem' => $list->problem,
            'benefits' => $list->benefits,
            'created_at' => $list->created_at->diffForHumans(),
        ];
    }
    $response = [
    	'count' => $results->count(),
    	'data' => $items,
    ];

    return response()->json($response);
});

Route::get('/bug/reports', function (Request $request = null) {
    $search = trim($request->get('status'));

    $results = App\BugReport::with(['status'])->where('status_id', $search);
    $items = [];

    foreach ($results->get() as $list) {
        $items[] = [
            'id' => $list->id,
            'name' => $list->name,
            'summary' => $list->summary,
            'impact' => $list->impact,
            'details' => $list->details,
            'os' => $list->os,
            'browser' => $list->browser,
            'created_at' => $list->created_at->diffForHumans(),
        ];
    }
    $response = [
    	'count' => $results->count(),
    	'data' => $items,
    ];

    return response()->json($response);
});

Route::get('/feature/request', function (Request $request = null) {
    $search = trim($request->get('status'));

    $results = App\FeatureRequest::with(['status'])->where('status_id', $search);
    $items = [];

    foreach ($results->get() as $list) {
        $items[] = [
            'id' => $list->id,
            'name' => $list->name,
            'summary' => $list->summary,
            'problem' => $list->problem,
            'created_at' => $list->created_at->diffForHumans(),
        ];
    }
    $response = [
    	'count' => $results->count(),
    	'data' => $items,
    ];

    return response()->json($response);
});

Route::get('/dashboard/reports', function (Request $request = null) {

    $feature = App\FeatureRequest::with(['status'])->where('status_id', 1)->count();
    $project = App\ProjectRequest::with(['status', 'owner'])->where('status_id', 1)->count();
	$bug = App\BugReport::with(['status'])->where('status_id', 1)->count();
    $items = [];

    $response = [
    	'project' => $project,
    	'bug' => $bug,
    	'feature' => $feature,
    ];

    return response()->json($response);
});